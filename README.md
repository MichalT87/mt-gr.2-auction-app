# Simple Auction App
A simple Auction App that uses Spring, Hibernate, Postgres with a Restful api
created to work with an external Bank app over HTTP Rest API.

Swagger can be used to test all functionalities.
