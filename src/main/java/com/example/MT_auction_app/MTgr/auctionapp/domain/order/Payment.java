package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

import com.example.MT_auction_app.MTgr.auctionapp.shared.Auditable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.math.BigDecimal;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter(value = AccessLevel.PRIVATE)
public class Payment extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "payment_sequence")
    @SequenceGenerator(name = "payment_sequence")
    private long id;

    private long orderId;

    private long ownerId;

    private long ownerAccountId;

    private String clientAccountNumber;

    private BigDecimal amount;

    private String title;


    public static Payment generate(Order order) {
        Payment payment = new Payment();
        payment.orderId = order.getId();
        payment.ownerId = order.getClientId();
        payment.ownerAccountId = order.getClientAccountId();
        payment.clientAccountNumber = order.getOwnerAccountNumber();
        payment.amount = order.getUnitPrice().multiply(new BigDecimal(order.getQuantity()));
        payment.title = String.format("%d, %d", order.getId(), order.getClientId());
        return payment;
    }
}