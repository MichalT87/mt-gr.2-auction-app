package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
public class OrderCreatorCommand {

    @NonNull
    private long auctionId;

    @NonNull
    private final long clientId;

    @NonNull
    private final long clientAccountId;

    @NonNull
    private final int quantity;
}