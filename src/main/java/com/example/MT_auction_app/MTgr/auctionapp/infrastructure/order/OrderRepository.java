package com.example.MT_auction_app.MTgr.auctionapp.infrastructure.order;

import com.example.MT_auction_app.MTgr.auctionapp.domain.order.Order;
import com.example.MT_auction_app.MTgr.auctionapp.domain.order.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findAllByStatus(OrderStatus orderStatus);

    Order findOrderById(long id);
}