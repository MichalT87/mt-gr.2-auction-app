package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.Auction;
import com.example.MT_auction_app.MTgr.auctionapp.shared.Auditable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter(AccessLevel.PRIVATE)
@Builder
@Table(name = "ORDERS")
public class Order extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "order_sequence")
    @SequenceGenerator(name = "order_sequence")
    private long id;
    private long auctionId;
    private String ownerAccountNumber;
    private BigDecimal unitPrice;
    private long clientId;
    private long clientAccountId;
    private int quantity;
    @Setter
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    static Order generatePending(OrderCreatorCommand orderCreatorCommand, Auction auction) {
        Order order = Order.builder().auctionId(orderCreatorCommand.getAuctionId())
                .ownerAccountNumber(auction.getOwnerAccountNumber())
                .unitPrice(auction.getPrice())
                .clientId(orderCreatorCommand.getClientId())
                .clientAccountId(orderCreatorCommand.getClientAccountId())
                .quantity(orderCreatorCommand.getQuantity())
                .status(OrderStatus.PENDING)
                .build();
        return order;
    }

    void statusChanger() {
        this.status = OrderStatus.PAID;
    }
}