package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

public enum OrderStatus {
    PENDING, PAID
}