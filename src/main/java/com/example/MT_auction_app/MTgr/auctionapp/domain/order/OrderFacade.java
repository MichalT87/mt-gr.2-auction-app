package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderFacade {

    private final OrderCreator orderCreator;
    private final OrderRetrievalClient orderRetrievalClient;
    private final StatusChanger statusChanger;

    public void createOrder(OrderCreatorCommand orderCreatorCommand) {
        orderCreator.createOrderAndUpdateAuction(orderCreatorCommand);
    }

    public void createPayment(long orderId) {
        orderCreator.createPayment(orderId);
    }

    @Transactional
    public void changeStatus(long orderId) {
        Order payedOrder = orderRetrievalClient.findById(orderId);
        statusChanger.changeStatus(payedOrder);
    }

    @Transactional
    public List<Payment> getAllPaymentsByOrderStatus() {
        return orderCreator.getAllPaymentsByOrderStatus();
    }

    @Transactional
    public List<Payment> getAllPaymentsByOrderStatusPaid() {
        return orderCreator.getAllPaymentsByOrderStatusPaid();
    }
}