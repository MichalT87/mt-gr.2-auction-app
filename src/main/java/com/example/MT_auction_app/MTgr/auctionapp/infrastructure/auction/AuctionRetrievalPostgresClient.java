package com.example.MT_auction_app.MTgr.auctionapp.infrastructure.auction;

import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.Auction;
import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.AuctionException;
import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.AuctionRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuctionRetrievalPostgresClient implements AuctionRetrievalClient {

    private final AuctionRepository auctionRepository;

    @Override
    public Auction getActiveByIdOrThrow(long id) {
        Optional<Auction> optionalAuction = auctionRepository.findById(id);
        Auction auction = optionalAuction.orElseThrow(
                () -> AuctionException.auctionIdNotFound(id));
        if (!auction.isActive()) {
            throw AuctionException.auctionIsInactive(id);
        }
        return auction;
    }
}