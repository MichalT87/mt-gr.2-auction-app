package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class OrderStatusChanger implements StatusChanger {

    @Override
    public void changeStatus(Order order) {
        order.statusChanger();
    }
}