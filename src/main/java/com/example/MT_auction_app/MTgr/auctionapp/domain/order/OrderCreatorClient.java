package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

public interface OrderCreatorClient {

    void create(Order order);
}