package com.example.MT_auction_app.MTgr.auctionapp.domain.auction;

public interface AuctionCreatorClient {

    void create(Auction auction);
}