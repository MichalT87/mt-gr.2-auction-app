package com.example.MT_auction_app.MTgr.auctionapp.domain.auction;

public interface QuantityChanger {

    void reduceQuantity(Auction auction, int reduceByQty);
}