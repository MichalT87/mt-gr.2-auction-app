package com.example.MT_auction_app.MTgr.auctionapp.infrastructure.order;

import com.example.MT_auction_app.MTgr.auctionapp.domain.order.Order;
import com.example.MT_auction_app.MTgr.auctionapp.domain.order.Payment;
import com.example.MT_auction_app.MTgr.auctionapp.domain.order.PaymentSenderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PaymentRestSender implements PaymentSenderClient {

    private final RestTemplate restTemplate;
    private final String transferBankAppUrl;

    @Autowired
    public PaymentRestSender(RestTemplate restTemplate,
                             @Value("${send.payment.bankapp.url}") String transferBankAppUrl) {
        this.restTemplate = restTemplate;
        this.transferBankAppUrl = transferBankAppUrl;
    }

    @Override
    public void sendPayment(Order order) {
        HttpEntity<Payment> requestEntity = new HttpEntity<>(Payment.generate(order));
        restTemplate.exchange(transferBankAppUrl,
                HttpMethod.POST,
                requestEntity,
                Void.class);
    }
}