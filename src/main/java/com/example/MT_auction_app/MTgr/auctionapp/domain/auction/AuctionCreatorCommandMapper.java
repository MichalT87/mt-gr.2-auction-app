package com.example.MT_auction_app.MTgr.auctionapp.domain.auction;

import com.example.MT_auction_app.MTgr.auctionapp.api.auction.AuctionRequest;

public class AuctionCreatorCommandMapper {

    public static AuctionCreatorCommand createAuction(AuctionRequest auctionRequest) {
        return AuctionCreatorCommand.builder()
                .ownerAccountNumber(auctionRequest.getOwnerAccountNumber())
                .title(auctionRequest.getTitle())
                .description(auctionRequest.getDescription())
                .quantity(auctionRequest.getQuantity())
                .price(auctionRequest.getPrice())
                .expirationDays(auctionRequest.getExpirationDays())
                .build();
    }
}
