package com.example.MT_auction_app.MTgr.auctionapp.infrastructure.auction;

import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.Auction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuctionRepository extends JpaRepository<Auction, Long> {
}