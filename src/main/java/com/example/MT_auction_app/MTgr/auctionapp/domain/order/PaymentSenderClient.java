package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

public interface PaymentSenderClient {

    void sendPayment(Order order);
}