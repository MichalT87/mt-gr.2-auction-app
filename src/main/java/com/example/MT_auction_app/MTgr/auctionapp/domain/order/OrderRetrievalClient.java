package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

import java.util.List;

public interface OrderRetrievalClient {

    Order findById(long id);

    List<Order> getPendingOrders();

    List<Order> getPaidOrders();
}