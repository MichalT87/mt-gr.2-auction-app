package com.example.MT_auction_app.MTgr.auctionapp.infrastructure.order;

import com.example.MT_auction_app.MTgr.auctionapp.domain.order.Order;
import com.example.MT_auction_app.MTgr.auctionapp.domain.order.OrderRetrievalClient;
import com.example.MT_auction_app.MTgr.auctionapp.domain.order.OrderStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderRetrievalPostgresClient implements OrderRetrievalClient {

    private final OrderRepository orderRepository;

    @Override
    public Order findById(long orderId) {
        return orderRepository.findOrderById(orderId);
    }

    @Override
    public List<Order> getPendingOrders() {
        return orderRepository.findAllByStatus(OrderStatus.PENDING);
    }

    @Override
    public List<Order> getPaidOrders() {
        return orderRepository.findAllByStatus(OrderStatus.PAID);
    }
}