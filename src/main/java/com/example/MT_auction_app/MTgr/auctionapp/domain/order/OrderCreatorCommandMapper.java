package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

import com.example.MT_auction_app.MTgr.auctionapp.api.order.OrderRequest;

public class OrderCreatorCommandMapper {

    public static OrderCreatorCommand createOrderCommand (long auctionId, OrderRequest orderRequest) {
        return OrderCreatorCommand.builder()
                .auctionId(auctionId)
                .clientId(orderRequest.getClientId())
                .clientAccountId(orderRequest.getClientAccountId())
                .quantity(orderRequest.getQuantity())
                .build();
    }
}