package com.example.MT_auction_app.MTgr.auctionapp.infrastructure.auction;

import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.Auction;
import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.AuctionUpdateClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class AuctionPostgresUpdateClient implements AuctionUpdateClient {

    private final AuctionRepository auctionRepository;

    public void update(Auction auction) {
        auctionRepository.save(auction);
    }
}