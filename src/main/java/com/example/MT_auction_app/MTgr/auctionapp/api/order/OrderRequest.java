package com.example.MT_auction_app.MTgr.auctionapp.api.order;

import lombok.Value;
import javax.validation.constraints.Min;

@Value
public class OrderRequest {

    @Min(1)
    private final long clientId;

    @Min(1)
    private final long clientAccountId;

    @Min(1)
    private final int quantity;

}