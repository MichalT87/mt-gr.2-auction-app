package com.example.MT_auction_app.MTgr.auctionapp.infrastructure.auction;

import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.Auction;
import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.AuctionCreatorClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuctionPostgresCreatorClient implements AuctionCreatorClient {

    private final AuctionRepository auctionRepository;

    @Override
    public void create(Auction auction) {
        auctionRepository.save(auction);
    }
}