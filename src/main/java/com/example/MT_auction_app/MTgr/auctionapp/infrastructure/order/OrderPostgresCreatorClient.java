package com.example.MT_auction_app.MTgr.auctionapp.infrastructure.order;

import com.example.MT_auction_app.MTgr.auctionapp.domain.order.OrderCreatorClient;
import com.example.MT_auction_app.MTgr.auctionapp.domain.order.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderPostgresCreatorClient implements OrderCreatorClient {

    private final OrderRepository orderRepository;

    @Override
    public void create(Order order) {
        orderRepository.save(order);
    }
}