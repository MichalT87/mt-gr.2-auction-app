package com.example.MT_auction_app.MTgr.auctionapp.api.order;

import com.example.MT_auction_app.MTgr.auctionapp.domain.order.OrderCreatorCommand;
import com.example.MT_auction_app.MTgr.auctionapp.domain.order.OrderCreatorCommandMapper;
import com.example.MT_auction_app.MTgr.auctionapp.domain.order.OrderFacade;
import com.example.MT_auction_app.MTgr.auctionapp.domain.order.Payment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderController {

    private final OrderFacade orderFacade;

    @PostMapping(path = "/auctions/{auctionId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void createOrder(@Min(1) @PathVariable long auctionId, @Valid @RequestBody OrderRequest orderRequest) {
        OrderCreatorCommand orderCreatorCommand = OrderCreatorCommandMapper.createOrderCommand(auctionId, orderRequest);
        orderFacade.createOrder(orderCreatorCommand);
        log.info("New order to auction with id {} has been created.", auctionId);
    }

    @PostMapping(path = "/{orderId}/payment")
    public void createPayment(@PathVariable long orderId) {
        orderFacade.createPayment(orderId);
        log.info("Payment to order with id {} has been created.", orderId);
    }

    @GetMapping(path = "/pending")
    public ResponseEntity<List<Payment>> getPendingOrders() {
        log.info("Getting orders with status: PENDING.");
        return ResponseEntity.ok(orderFacade.getAllPaymentsByOrderStatus());
    }

    @GetMapping(path = "/paid")
    public ResponseEntity<List<Payment>> getPaidOrders() {
        log.info("Getting orders with status: PAID.");
        return ResponseEntity.ok(orderFacade.getAllPaymentsByOrderStatusPaid());
    }

    @PostMapping(path = "/{orderId}/status")
    @ResponseStatus(HttpStatus.OK)
    public void changeOrderStatus(@PathVariable long orderId) {
        orderFacade.changeStatus(orderId);
    }
}