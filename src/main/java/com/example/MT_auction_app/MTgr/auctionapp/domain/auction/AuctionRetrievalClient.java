package com.example.MT_auction_app.MTgr.auctionapp.domain.auction;

public interface AuctionRetrievalClient {

    Auction getActiveByIdOrThrow(long id);
}