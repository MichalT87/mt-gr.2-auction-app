package com.example.MT_auction_app.MTgr.auctionapp.domain.auction;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
class AuctionCreator {

    private final AuctionCreatorClient auctionCreatorClient;
    private final AuctionScheduler auctionScheduler;

    @Transactional
    public void create(AuctionCreatorCommand auctionCreatorCommand) {
        Auction auction = Auction.generateActive(auctionCreatorCommand);
        auctionCreatorClient.create(auction);
        auctionScheduler.scheduleDeactivation(auction.getId(), auction.getEndDate());
        log.info("New auction: *{}: {}* has been created.", auctionCreatorCommand.getTitle(), auctionCreatorCommand.getDescription());
    }
}