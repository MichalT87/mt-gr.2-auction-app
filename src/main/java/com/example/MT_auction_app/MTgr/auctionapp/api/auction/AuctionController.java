package com.example.MT_auction_app.MTgr.auctionapp.api.auction;

import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.AuctionCreatorCommand;
import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.AuctionCreatorCommandMapper;
import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.AuctionFacade;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
@RequestMapping("/auctions")
@RequiredArgsConstructor
public class AuctionController {

    private final AuctionFacade auctionFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createAuction(@Valid @RequestBody AuctionRequest auctionRequest) {
        AuctionCreatorCommand auctionCreatorCommand = AuctionCreatorCommandMapper.createAuction(auctionRequest);
        auctionFacade.createAuction(auctionCreatorCommand);
    }
}