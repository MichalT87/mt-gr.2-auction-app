package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.Auction;
import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.AuctionRetrievalClient;
import com.example.MT_auction_app.MTgr.auctionapp.domain.auction.QuantityChanger;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class OrderCreator {

    private final AuctionRetrievalClient auctionRetrievalClient;
    private final OrderCreatorClient orderCreatorClient;
    private final QuantityChanger quantityChanger;
    private final OrderRetrievalClient orderRetrievalClient;
    private final PaymentSenderClient paymentSenderClient;

    @Transactional
    public void createOrderAndUpdateAuction(OrderCreatorCommand orderCreatorCommand) {
        Auction auction = auctionRetrievalClient.getActiveByIdOrThrow(orderCreatorCommand.getAuctionId());
        if (auction.getQuantity() >= orderCreatorCommand.getQuantity()) {
            Order order = Order.generatePending(orderCreatorCommand, auction);
            orderCreatorClient.create(order);
            quantityChanger.reduceQuantity(auction, order.getQuantity());
        } else {
            throw new IllegalArgumentException(String
                    .format("Order quantity is higher then auction quantity [Order quantity = %d] [Auction quantity = %d]",
                            orderCreatorCommand.getQuantity(),
                            auction.getQuantity()));
        }
    }

    @Transactional
    public void createPayment(long orderId) {
        Order payOrder = orderRetrievalClient.findById(orderId);
        if (payOrder.getStatus().equals(OrderStatus.PENDING)) {
            paymentSenderClient.sendPayment(payOrder);
        } else {
            throw new IllegalArgumentException(String
                    .format("Order with ID number: %d has been already paid.",
                            payOrder.getId()));
        }
    }

    @Transactional
    public List<Payment> getAllPaymentsByOrderStatus() {
        return orderRetrievalClient.getPendingOrders()
                .stream()
                .map(Payment::generate)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<Payment> getAllPaymentsByOrderStatusPaid() {
        return orderRetrievalClient.getPaidOrders().stream()
                .map(Payment::generate)
                .collect(Collectors.toList());
    }
}