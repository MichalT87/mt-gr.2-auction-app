package com.example.MT_auction_app.MTgr.auctionapp.domain.auction;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuctionFacade {

    private final AuctionCreator auctionCreator;

    public void createAuction(AuctionCreatorCommand auctionCreatorCommand) {
        auctionCreator.create(auctionCreatorCommand);
    }
}