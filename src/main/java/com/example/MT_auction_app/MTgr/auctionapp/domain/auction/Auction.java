package com.example.MT_auction_app.MTgr.auctionapp.domain.auction;

import com.example.MT_auction_app.MTgr.auctionapp.shared.Auditable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter(AccessLevel.PRIVATE)
@Table(name = "AUCTIONS")
public class Auction extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auction_sequence")
    @SequenceGenerator(name = "auction_sequence")
    private long id;
    private String ownerAccountNumber;
    private String title;
    private String description;
    private int quantity;
    private BigDecimal price;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private boolean active;

    static Auction generateActive(AuctionCreatorCommand auctionCreatorCommand) {
        LocalDateTime currentDateTime = LocalDateTime.now();
        Auction auction = new Auction();
        auction.ownerAccountNumber = auctionCreatorCommand.getOwnerAccountNumber();
        auction.title = auctionCreatorCommand.getTitle();
        auction.description = auctionCreatorCommand.getDescription();
        auction.quantity = auctionCreatorCommand.getQuantity();
        auction.price = auctionCreatorCommand.getPrice();
        auction.startDate = currentDateTime;
        auction.endDate = auction.startDate.plusDays(auctionCreatorCommand.getExpirationDays());
        auction.active = true;
        return auction;
    }

    void reduceQuantityAndVerifyStatus(int reduceByQuantity) {
        this.quantity -= reduceByQuantity;
        if (this.quantity == 0) {
            deactivate();
        }
    }

    void deactivate() {
        this.active = false;
    }
}