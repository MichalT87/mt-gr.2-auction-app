package com.example.MT_auction_app.MTgr.auctionapp.domain.auction;

public interface AuctionUpdateClient {

    void update(Auction auction);
}
