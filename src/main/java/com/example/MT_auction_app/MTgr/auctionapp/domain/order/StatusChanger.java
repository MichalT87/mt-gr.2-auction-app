package com.example.MT_auction_app.MTgr.auctionapp.domain.order;

public interface StatusChanger {

    void changeStatus(Order order);
}